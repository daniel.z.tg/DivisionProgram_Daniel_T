package me.danielzgtg.compsci11_sem2_2017.divisionprogram;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * A program to perform integer or floating-point division.
 * 
 * @author Daniel Tang
 * @since 6 February 2017
 */
public final class DivisionProgram {

	/**
	 * The prompt layout for dividend input.
	 */
	private static final String DIVIDEND_PROMPT = "What is the dividend?: ";

	/**
	 * The prompt layout for divisor input.
	 */
	private static final String DIVISOR_PROMPT = "What is the divisor?: ";

	/**
	 * The error message when the user did not enter a real number.
	 */
	private static final String NUMBER_PARSE_ERROR = "Sorry! I didn't understand that number, please try again.";

	/**
	 * The prompt layout for integer/float division selection.
	 */
	private static final String DIVISION_TYPE_PROMPT = "What kind of division would you like to perform? (integer/float): ";

	/**
	 * The lowercase list of aliases the user could choose floating-point division with, case insensitive.
	 */
	private static final /*immutable*/ List<String> FLOAT_DIVISION_NAMES =
			Collections.unmodifiableList(Arrays.asList(new String[] { "f", "float", "double", "decimal", "floating" }));

	/**
	 * The lowercase list of aliases the user could choose integer division with, case insensitive.
	 */
	private static final /*immutable*/ List<String> INTEGER_DIVISION_NAMES =
			Collections.unmodifiableList(Arrays.asList(new String[] { "i", "integer", "int", "wholenumber", "whole" }));

	/**
	 * The error message when the user did not specify the type of divison clearly.
	 */
	private static final String DIVISION_TYPE_ERROR =
			"Sorry! I still don't know which type of divison you're talking about, please try again.";

	/**
	 * The output layout for the result.
	 */
	private static final String OUTPUT_FORMAT = "The quotient is: %s";

	/**
	 * The answer when we divide by zero.
	 */
	private static final String DIVISON_BY_ZERO_QUOTIENT = "'undefined.'";

	public static final void main(final String[] ignore) {
		final String textDividend, textDivisor, quotient;
		final boolean useFloatDivision;

		// Prompt console user for dividend, divisor, and type of division, and obtain it.
		try (final Scanner scanner = new Scanner(System.in)) {
			textDividend = promptNumberStringInput(scanner, DIVIDEND_PROMPT);
			textDivisor = promptNumberStringInput(scanner, DIVISOR_PROMPT);
			useFloatDivision = promptIsFloatDivision(scanner);
		}

		// Perform either type of division
		if (useFloatDivision) {
			quotient = calcFloatDivision(textDividend, textDivisor);
		} else {
			quotient = calcIntDivision(textDividend, textDivisor);
		}

		// Print out the result
		System.out.format(OUTPUT_FORMAT, quotient);
	}

	/**
	 * Parses a dividend and divisor as a float, then calculates and returns the result of division.
	 * 
	 * @param textDividend The dividend to use for division.
	 * @param textDivisor The dividend to use for division.
	 * @return Quotient result of division as string, or "undefined" if divisor is 0.
	 */
	private static final String calcFloatDivision(final String textDividend, final String textDivisor) {
		final float dividend, divisor;

		// Parse the number
		dividend = Float.parseFloat(textDividend);
		divisor = Float.parseFloat(textDivisor);

		// Calculate and return the result
		return divisor == 0.0F ? DIVISON_BY_ZERO_QUOTIENT : String.valueOf(dividend / divisor);
	}

	/**
	 * Parses a dividend and divisor as a integer (truncated if decimal),
	 * then calculates and returns the result of division.
	 * 
	 * @param textDividend The dividend to use for division.
	 * @param textDivisor The dividend to use for division.
	 * @return Quotient result of division as string, or "undefined" if divisor is 0.
	 */
	private static final String calcIntDivision(final String textDividend, final String textDivisor) {
		final int dividend, divisor;

		// Parse the number
		{
			int dividendTry = 0, divisorTry = 0;

			try {
				dividendTry = Integer.parseInt(textDividend);
				divisorTry = Integer.parseInt(textDivisor);
			} catch (NumberFormatException nfe) {
				/*
				 * Assume the user wants truncated decimals.
				 * 
				 * This is to let the user input numbers like 1.0.
				 * If it was really was not a whole number,
				 * they should have known that before choosing integer division.
				 */
				dividendTry = (int) Double.parseDouble(textDividend);
				divisorTry = (int) Double.parseDouble(textDivisor);
			}

			dividend = dividendTry;
			divisor = divisorTry;
		}

		// Calculate and return the result
		return divisor == 0 ? DIVISON_BY_ZERO_QUOTIENT : String.valueOf(dividend / divisor);
	}

	/**
	 * Determined whether the user wants floating-point or integer division.
	 * 
	 * @param scanner The {@link Scanner} to use.
	 * @return {@code true} if the user wants floating-point division,
	 * {@code false} if the user wants integer division.
	 */
	private static final boolean promptIsFloatDivision(final Scanner scanner) {
		/*variable*/ String userInput;

		// Prompt the user for the division type, and try to determine their intention
		while (true) {
			System.out.print(DIVISION_TYPE_PROMPT);
			userInput = scanner.nextLine().toLowerCase();

			if (FLOAT_DIVISION_NAMES.contains(userInput)) {
				return true;
			} /*else*/ if (INTEGER_DIVISION_NAMES.contains(userInput)) {
				return false;
			} /*else {*/

			// Try again if we weren't able to
			System.out.println(DIVISION_TYPE_ERROR);
			/*}*/
		}
	}

	/**
	 * Obtains a single numeric {@code String} from the user.
	 * 
	 * @param scanner The {@link Scanner} to use.
	 * @param prompt The {@code String} to prompt the user with for input.
	 * @return The obtained numeric string.
	 */
	private static final String promptNumberStringInput(final Scanner scanner, final String prompt) {
		/*variable*/ String userInput;

		// Prompt the user for a number
		while (true) {
			System.out.print(prompt);
			userInput = scanner.nextLine();

			if (isRealNumeric(userInput)) {
				return userInput;
			} /*else {*/

			// Try again if we don't understand the number
			System.out.println(NUMBER_PARSE_ERROR);
			/*}*/
		}
	}

	/**
	 * Check if the {@code String} can be understood as a real number.
	 * 
	 * @param s The {@code String} to check.
	 * @return {@code true} if we can understand the {@code String}, {@code false} otherwise.
	 */
	private static final boolean isRealNumeric(final String s) {
		final Float f;
		try {
			f = new Float(s);
			// We understand it if it is a real number.
			return !f.isNaN() && !f.isInfinite();
		} catch (final NumberFormatException nfe) {
			// We don't understand it
			return false;
		}
	}

	private DivisionProgram() { throw new UnsupportedOperationException("Not Instantiatable!"); }
}
